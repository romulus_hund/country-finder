import React from 'react';
import style from './CountriesList.module.scss';
import {Link} from 'react-router-dom';
import {Input} from 'antd';

const {Search} = Input;

const CountriesList = ({countries, handleOnClick, handleOnSearchChange}) => {
    return (
        <div className={style['countries-list-wrapper']}>
            <div className="search-item-wrapper">
                <Search
                    placeholder="search country"
                    onChange={handleOnSearchChange}
                />
            </div>
            <ul>
                {countries.map(country => (
                        <li key={country.alpha2Code} onClick={handleOnClick}>
                            <Link to={{
                                pathname: `${country.alpha2Code}`,
                                state: {country}
                            }}>
                                {country.name}
                            </Link>
                        </li>
                    )
                )
                }
            </ul>
        </div>
    );

};

export default CountriesList;

