import React, {Component} from 'react';
import style from './Sidebar.module.scss';
import {alphabet, countries, isLoading} from '../../store/selectors';
import {setFilterLetter, updateSearch} from '../../store/actions/countriesActions';
import {connect} from 'react-redux';

class Sidebar extends Component {

    handleOnClick = letter => {
        const {setFilterLetter, updateSearch} = this.props;
        setFilterLetter(letter);
        updateSearch('');
    };

    render() {
        const {alphabet} = this.props;
        return (
            <div className={style['sidebar-wrapper']}>
                <div className='alphabet'>
                    {
                        alphabet.map((char, i) =>
                            <button
                                onClick={() => this.handleOnClick(char)}
                                key={i}
                                className='letter'
                            >
                                {char.toUpperCase()}
                            </button>)
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: isLoading(state),
        countries: countries(state),
        alphabet: alphabet(state)
    };
};

function mapDispatchToProps(dispatch) {
    return {
        setFilterLetter: data => dispatch(setFilterLetter(data)),
        updateSearch: data => dispatch(updateSearch(data))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Sidebar);

