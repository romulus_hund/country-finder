import axios_based from './axios-base';

export function fetchAllCountriesApi() {
    return axios_based.get('/all');
}