import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL || 'https://restcountries.eu/rest/v2/'
  });

export default instance;
