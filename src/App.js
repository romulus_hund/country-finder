import React, {Component} from 'react';
import style from './App.module.scss';
import PageLayout from './components/pageLayout';
import { Button, Input } from 'antd';
import {connect} from 'react-redux';
import {fetchAllCountries} from './store/actions/countriesActions';
import { Route, Switch } from 'react-router-dom';
import MainPage from './pages/main/MainPage';
import CountryDetails from './pages/countryDetails/CountryDetails';

const { Search } = Input;
class App extends Component {

  componentDidMount() {
    this.props.fetchAllCountries();

  }

  render() {
    return (
      <div className={style['App']}>
        <PageLayout>
          <header className="App-header">
            <h2>COUNTRY FINDER</h2>
          </header>

          <Switch>
            <Route path="/" exact component={MainPage} />
            <Route path="/:countryCode" exact component={CountryDetails} />
          </Switch>
        </PageLayout>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAllCountries: () => dispatch(fetchAllCountries())
  };
}

export default  connect(
    null,
    mapDispatchToProps
  )(App);

