import { createSelector } from 'reselect';

export const isLoading = state => state?.countriesReducer?.isLoading;

export const filterLetter = state => state?.countriesReducer?.filterLetter;

export const countries = state => state?.countriesReducer?.countries;

export const alphabet = createSelector(countries, (countries) => {
    const newCountries = Object.assign(countries);
   const arr =[];
    newCountries.map(country => {
        arr.push(country.name.charAt(0));
    });
    return [...new Set(arr)].sort();

});

export const getSearchQuery = state => state?.countriesReducer?.searchQuery;

export const filteredCountries = createSelector(filterLetter, countries, getSearchQuery, (filterLetter, countries, searchQuery) => {
    const filteredCountries = filterLetter ? countries.filter(country => country.name.charAt(0) === filterLetter) : countries;
    return filteredCountries.filter(country => country.name.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1);
});

