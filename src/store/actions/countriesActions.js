import types from '../actionsTypes';

export const fetchAllCountries = data =>{
  return{
    type: types.FETCH_COUNTRIES,
    payload: data
  };
};

export const setLoading = data => {
  return{
    type: types.SET_LOADING,
    payload: data
  };
};

export const setCountriesToStore = data =>{
  return{
    type: types.SET_COUNTRIES_TO_STORE,
    payload: data
  };
};

export const setFilterLetter = data =>{
  return{
    type: types.SET_FILTER_LETTER,
    payload: data
  };
};

export const updateSearch = data =>{
  return{
    type: types.UPDATE_SEARCH,
    payload: data
  };
};

