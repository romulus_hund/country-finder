import actionsTypes from '../actionsTypes';
import createReducer from '../reducers/createReducer';

const initialState = {
  countries: [],
  isLoading: false,
  error: null,
  filterLetter: '',
  searchQuery: ''
};

const countriesReducer = createReducer(initialState, {
  [actionsTypes.SET_LOADING]: (state, {payload}) => {
    return {
      ...state,
      isLoading: payload
    };
  },
  [actionsTypes.SET_COUNTRIES_TO_STORE]: (state, {payload}) => {
    return {
      ...state,
      countries: payload
    };
  },
  [actionsTypes.FETCH_COUNTRIES_FAILED]: (state, {payload}) => {
    return {
      ...state,
      error: payload
    };
  },
  [actionsTypes.SET_FILTER_LETTER]: (state, {payload}) => {
    return {
      ...state,
      filterLetter: payload
    };
  },
  [actionsTypes.UPDATE_SEARCH]: (state, {payload}) => {
    return {
      ...state,
      searchQuery: payload
    };
  },
});

export default countriesReducer;

