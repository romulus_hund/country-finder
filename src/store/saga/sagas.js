import { call, put } from 'redux-saga/effects';
import types from '../actionsTypes';
import { setCountriesToStore, setLoading } from '../actions/countriesActions';
import {fetchAllCountriesApi} from '../../api';

export function* fetchAllCountriesSaga(action) {
  try {
    yield put(setLoading(true));

    const result = yield call(fetchAllCountriesApi, action.payload);

    if (result?.status === 200) {
      yield put(setCountriesToStore(result.data));
    }

    yield put(setLoading(false));

  } catch (error) {
    console.log('---err: ', error);
    yield put({ type: types.FETCH_COUNTRIES_FAILED, payload: error.message });
  }
}
