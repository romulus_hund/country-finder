import {takeEvery} from 'redux-saga/effects';
import types from '../actionsTypes';
import {fetchAllCountriesSaga} from '../saga/sagas';

export function* watchSaga() {
  yield takeEvery(types.FETCH_COUNTRIES, fetchAllCountriesSaga);
}
