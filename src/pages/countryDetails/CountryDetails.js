import React from 'react';
import style from './CountryDetails.module.scss';
import {Link} from 'react-router-dom';
import {Card} from 'antd';

const CountryDetails = props => {
    const {country} = props.location.state;
    const {alpha2Code, capital, population, name, flag} = country;

    return (
        <div className={style['country-details-wrapper']}>
            <div className="country-details">
                <Card title={name.toUpperCase()} bordered style={{width: 300}}>
                    <div className='flag'>
                        <img src={flag}/>
                    </div>
                    <div className='detail-item'>Capital: <b>{capital}</b></div>
                    <div className='detail-item'>Population: <b>{population}</b></div>
                    <div className='detail-item'>Alpha2Code: <b>{alpha2Code}</b></div>
                </Card>
            </div>

            <Link to='/'>
                <button>Go Back</button>
            </Link>

        </div>
    );

}

export default CountryDetails;

