import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchAllCountries, setFilterLetter, updateSearch} from '../../store/actions/countriesActions';
import {isLoading, countries, filteredCountries} from '../../store/selectors';
import Spinner from '../../components/Spinner';
import Sidebar from '../../components/sidebar/Sidebar';
import CountriesList from '../../components/countriesList/CountriesList';

class MainPage extends Component {

    componentDidMount() {
        this.props.fetchAllCountries();

    }

    handleOnClick = () => {
        this.props.setFilterLetter('');
    };

    handleOnSearchChange = e => {
        const { updateSearch } = this.props;
        updateSearch(e.target.value);
    };

    render() {
        const {isLoading, setFilterLetter, filteredCountries} = this.props;
        return (

            <div className="main-page-content">
                {
                    isLoading ? <Spinner/> :
                        <Fragment>
                            <Sidebar setFilterLetter={setFilterLetter}/>
                            <CountriesList
                                countries={filteredCountries}
                                handleOnClick={this.handleOnClick}
                                handleOnSearchChange={this.handleOnSearchChange}
                            />
                        </Fragment>
                }

            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: isLoading(state),
        countries: countries(state),
        filteredCountries: filteredCountries(state)
    };
};

function mapDispatchToProps(dispatch) {
    return {
        fetchAllCountries: () => dispatch(fetchAllCountries()),
        setFilterLetter: data => dispatch(setFilterLetter(data)),
        updateSearch: data => dispatch(updateSearch(data))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainPage);

